from app.controller import email_controller
from app.controller.firebase_controller import send_push_notification
from app.model.notification_model import NotificationModel
from fastapi import FastAPI
from fastapi import Request


import httpx

app = FastAPI()

@app.post("/webhooks")
async def handle_webhooks(request: Request): 
    payload = NotificationModel.parse_obj(await request.json())
    
    await send_push_notification("Event occured", payload)
    return {"status": "success"}


# @app.post("/webhooks/summary")
# async def handle_webhooks(request: Request): 
#     payload = NotificationModel.parse_obj(await request.json())

#     summary = await request_gemeni_summorization(payload)
#     await send_email("Daily Summary", summary)

#     return {"status": "success"}

