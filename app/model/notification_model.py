from .field_model import FieldModel
from pydantic import BaseModel, validator
from typing import List, Union
import ast


class NotificationModel(BaseModel):
    title: str
    level: str
    timestamp: str
    tags: Union[str, List[str]]
    description: str
    fields: List[FieldModel]

    @validator('tags', pre=True)
    def parse_tags(cls, v):
        if isinstance(v, str):
            try:
                return ast.literal_eval(v)
            except (SyntaxError, ValueError):
                raise ValueError(f"Unable to parse {v} into a list")
        return v

    def to_dict(self):
       return self.dict()
    
    