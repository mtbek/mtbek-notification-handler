from pydantic import BaseModel
from typing import List, Union

class FieldModel(BaseModel):
    name: str
    value: Union[str, int]