import smtplib

def send_email(user, password, recipients, sender, message):
    try:
        session = smtplib.SMTP_SSL('smtp.zoho.com', 465)
        session.login(user, password)
        session.sendmail(sender, recipients, message)
        session.quit()

        print("Email sent successfully!")
        
    except Exception as e:
        print("Failed to send email:", str(e))

if __name__ == "__main__":
    user = 'ldraper@XXXXXXX.net'
    password = 'XXXXXXX'
    recipients = ['5555555555@vtext.com']
    sender = 'ldraper@XXXXXXX.net'
    message = "test"

    send_email(user, password, recipients, sender, message)