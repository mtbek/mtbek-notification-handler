import firebase_admin
from firebase_admin import credentials
from firebase_admin import messaging
from app.model.notification_model import NotificationModel
import json
from datetime import datetime

cred = credentials.Certificate('mtbek_firebase.json')
firebase_admin.initialize_app(cred)

async def send_push_notification(title, body: NotificationModel):

    notificationContent = f"{body.title}\nTags: {', '.join(body.tags)}\n{body.description}\nLevel:{body.level}"

    message = messaging.Message(
        notification=messaging.Notification(
            title=title +" " + body.title,
            body=notificationContent
        ),
        topic='security-events'
    )

    messaging.send(message)