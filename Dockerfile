FROM python:3.9

WORKDIR /code

COPY ./requirements.txt /code/requirements.txt

# RUN python -m venv mtbek-notification-handler-venv
# RUN /bin/bash -c "source mtbek-notification-handler-venv/bin/activate"

RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY ./app /code/app

COPY mtbek_firebase.json /code/mtbek_firebase.json

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "455"]

EXPOSE 455